/**
 * Created by marlon.jerez on 28/04/2015.
 */


/* ########################################################
 MODEL AUOMIGRATION - UPDATES db tables,
 This script must be run manually because tables may loose all data
 ######################################################## */

var app = require("../server");
var modelNames = Object.keys(app.models);
modelNames.forEach(function (modelName) {
   var model = app.models[modelName];
   var ds = app.dataSources[model.dataSource.settings.name];
   var database = ds.settings.database;
   if (database) {
      where = null;
      model.count(where, function (err, count) {
         if (err) {
            console.error(err.message);
         }else{
            //if table exist and does not have any element, we may be updated automatically
            ds.isActual(modelName, function (err, actual) {
               if (!actual) {
                  ds.autoupdate(modelName, function (err, result) {
                     console.log('UPDATING TABLE "' + modelName + '" IN "' + database + '".');
                     process.exit();
                  });
               } else {
                  console.log('TABLE "' + modelName + '" IN "' + database + '" already updated.');
               }
            });
         }
      });
   }
});

