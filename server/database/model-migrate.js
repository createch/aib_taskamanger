/**
 * Created by marlon.jerez on 28/04/2015.
 */

process.env.MIGRATE = "MIGRATING MODELS";
var loopback = require('loopback');
var boot = require('loopback-boot');
var app = module.exports = loopback();


// Bootstrap the application, configure models, datasources and middleware.
// Sub-apps like REST API are mounted via boot scripts.
boot(app, __dirname + "/..", function (err) {

	/* ########################################################
	 MODEL MIGRATION - Create db tables if don't Exist
	 ######################################################## */


	var modelNames = Object.keys(app.models);
	var count = 0;
	modelNames.forEach(function (modelName) {

		var model = app.models[modelName];
		var ds = app.dataSources[model.dataSource.settings.name];
		var database = ds.settings.database;
		var connector = ds.settings.connector;
		if (database && connector !== "memory") {
			count++;
			ds.automigrate(modelName, function (err) {
				if (err){
					console.log('Error migrating: "' + modelName);
					throw err;
				}else{
					console.log('Table: "' + modelName + '" Created IN Database: "' + database + '".');
				}

				count--;
				if (count === 0) {
					process.exit(code = 0);
				}
			});
		}
	});
});
