var validator = require('validator');
module.exports = function (app) {
	var loopback = app.loopback;
	var privateRouter = loopback.Router();


	/**
	 * Identifies the Current User Making the request
	 */
	privateRouter.use(function (req, res, next) {

		var AccessToken = app.models.AccessToken;
		var Role = app.models.Role;

		var returnError = function(res){
			var error ={
				"error": {
					"name": "Error",
					"status": 401,
					"message": "Authorization Required",
					"statusCode": 401,
					"code": "AUTHORIZATION_REQUIRED"
				}
			};
			return res.status(401).send(error);
		};


		AccessToken.findForRequest(req,{}, function (err, token) {
			if(err) throw err;
			if(token){
				console.log(token);
				Role.findOne({
					where:{id:token.userId,name:"admin"}
				}, function(err, role) {
					if(err) throw err;
					if(role){
						next();
					}else{
						return returnError(res);
					}
				});
			}else{
				return returnError(res);
			}

		});

		//var token = req.accessToken || req.signedCookies.authorization;




	});

	app.use("/private",privateRouter);


	app.get("/schemas/:model",function (req, res) {
		var model= validator.toString(req.params.model);
		var alpha = validator.isAlpha(model);
		if(alpha && app.models[model]){
			res.json({
				name : app.models[model].definition.name,
				properties : app.models[model].definition.properties,
				relations : app.models[model].settings.relations
			});
		}else{
			res.status(404).json({error:{
				name : "Error",
				status : 404,
				message : 'Unknown Model "'+model+'"',
				"code":"MODEL_NOT_FOUND"
			}})
		}
	});

};
