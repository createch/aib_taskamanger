module.exports = function (app) {


	// avoid data initalization on migrate script
	if (process.env.MIGRATE) {
		console.log("Model Migration In Process => Skipping Models Initialization.");
		return;
	}

	/* ########################################################
	 INITIALIZE  DATA
	 ######################################################## */

	var Role = app.models.Role;
	var Member = app.models.Member;
	var RoleMapping = app.models.RoleMapping;
	var Agency = app.models.Agency;
	var SocialChannel = app.models.SocialChannel;
	var Category = app.models.Category;


	//create default channels
	var ch = [
		{"name": "facebook", "icon": "<i class='fa fa-facebook-official'></i>", "color": "#3b5998"},
		{"name": "twitter", "icon": "<i class='fa fa-twitter'></i>", "color": "#55acee"}
	];
	ch.forEach(function (channel) {
		SocialChannel.findOrCreate({where: {name: channel.name}}, channel, function (err, result) {
			if (err) console.error(channel + err);
			console.log("Chanel Created: " + result.name);
		});
	});

	// create default categories
	var cat = [
		{"name": "mortgages", "color": "#F16422"},
		{"name": "business", "color": "#A3479A"},
		{"name": "personal", "color": "#2899E5"},
		{"name": "gaa", "color": "#51B748"},
		{"name": "backing brave", "color": "#ED2843"}
	];
	cat.forEach(function (category) {
		Category.findOrCreate({where: {name: category.name}}, category, function (err, result) {
			if (err) console.error(category + err);
			console.log("Category Created: " + result.name);
		});
	});

	//create agencies channels
	var agencies = [
		{"name": "radical", "color": "#000000", "id": 1},
		{"name": "rothco", "color": "#009900", "id": 2},
		{"name": "whpr", "color": "#CA281D", "id": 3}
	];

	agencies.forEach(function (agency) {
		Agency.findOrCreate({where: {name: agency.name}}, agency, function (err, result) {
			if (err) console.error(agency + err);
			console.log("Agency Created: " + result.name);
		});
	});


	//create members
	var members = [
		{
			"username": "Admin",
			"email": "creativetechnology@radical.ie",
			"password": "pass",
			"id": 1,
			"agencyId": 1,
			"adminAgencyId": 1
		},
		{
			"username": "admin-radical",
			"email": "aib-task@radical.ie",
			"password": "hello-radical",
			"id": 2,
			"agencyId": 1,
			"adminAgencyId": 1
		},
		{
			"username": "admin-rothco",
			"email": "aib-task@rothco.ie",
			"password": "hello-rothco",
			"id": 3,
			"agencyId": 2,
			"adminAgencyId": 2
		},
		{
			"username": "admin-whpr",
			"email": "aib-task@wilsonhartnell.ie",
			"password": "hello-whpr",
			"id": 4,
			"agencyId": 3,
			"adminAgencyId": 3
		}
	];

	members.forEach(function (member) {
		Member.findOrCreate({where: {email: member.email}}, member, function (err, result) {
			if (err) throw err;
			console.log("Member Created: " + result.username);
		});
	});

	//CREATE Admin user
	var createAdminRole = function createAdminRole(user) {
		//create the admin role
		Role.create({
			name: 'admin'
		}, function (err, role) {
			if (err) throw err;
			//make bob an admin
			role.principals.create({
				principalType: RoleMapping.USER,
				principalId: user.id
			}, function (err, principal) {
				if (err) throw err;
				console.log("#######################################################");
				console.log('Admin Created: {username:"' + user.username + '" , email:"' + user.email + '" , "id":"' + user.id + '"}');
				console.log('Created principal:', principal);
				console.log("#######################################################");
			});
		});
	};
		createAdminRole(members[0]);


};
