module.exports = function enableAuthentication(server) {
	// enable authentication
	server.enableAuth();


	server.models.Member.afterRemote("login", function(context, result, next) {
		var req, res;
		req = context.req, res = context.res;
		if (result != null) {
			var expires = 0;
			if (req.body && req.body.rememberMe)
				expires = new Date(Date.now() + 63118441753 ) ;

			if (result.id != null) {
				res.cookie("authorization",result.id, {
					httpOnly: true,
					signed: true,
					expires:expires
				});
			}
			if (result.userId != null) {
				res.cookie("app_userId",result.userId, {
					httpOnly: false,
					signed: false,
					expires:expires
				});
			}
		}
		if(req.get('Content-Type') === "application/x-www-form-urlencoded") {
			if (req.body && req.body.successUrl){
				res.redirect(req.body.successUrl);
			}
		}
		return next();
	});

	server.models.Member.afterRemoteError("login", function(context,  next) {
		var req, res;
		req = context.req, res = context.res;
		if(req.get('Content-Type') === "application/x-www-form-urlencoded") {
			if (req.body && req.body.errorUrl){
				res.redirect(req.body.errorUrl);
				return;
			}
		}
		return next();
	});

	server.models.Member.afterRemote("logout", function(context, result, next) {
		var req, res;
		req = context.req, res = context.res;
		res.clearCookie("authorization");
		res.clearCookie("app_userId");
		return next();
	});

};
