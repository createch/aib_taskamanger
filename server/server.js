var loopback = require('loopback');
var boot = require('loopback-boot');
var cookieParser = require("cookie-parser");
var session = require('cookie-session');
var lusca = require('lusca');
var expressValidator = require('express-validator');


//uncomment to see debug ACL Messages
//process.env.DEBUG = "loopback:security:*";

/**
 * MIDDLEWARE INITIALIZATION
 */
var app = module.exports = loopback();
var keys = ["I+EPuH6igDm91P_980yGxM", "3;Z98E_~7+94C6-q70q91I", "SZ0J91p2v~caA87Dtu9QCz", ":-t~.*LQV5AD^_m9f_.K9C", "l!85+1_pd!.3-~.1AsN_VI"];
app.use(session({keys: keys}));
app.use(lusca({
	csrf: false,
	csp: false,
	xframe: 'SAMEORIGIN',
	p3p: false,
	hsts: false,
	xssProtection: true
}));
app.use(cookieParser(keys[0]));
app.use(loopback.token({model: app.models.accessToken}));
app.use(loopback.context());




/**
 * BOOT AND SERVER START
 */
app.start = function () {
	// start the web serve
	//var port = process.env.PORT || 3000;
	return app.listen(function () {
		app.emit('started');
		console.log('Web server listening at: %s', app.get('url'));
	});
};

// Bootstrap the application, configure models, datasources and middleware.
// Sub-apps like REST API are mounted via boot scripts.
boot(app, __dirname, function (err) {
	if (err) throw err;

	// start the server if `$ node server.js`
	if (require.main === module)
		app.start();
});
