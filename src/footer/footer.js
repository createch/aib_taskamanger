/**
 * Created by marlon.jerez on 07/05/2015.
 */

FooterView = Backbone.View.extend({
	el: "#Footer",
	initialize: function () {
		this.position();
		_.bindAll(this, 'position');
		$(window).on("load",this.position);
		$(window).on("resize",this.position);
	},
	position: function () {
		var footer = this.$el;
		if(!footer) return; /// fix a bug where footer is not defined on page load
		var pos = footer.position();
		var height = $(window).height();
		height = height - pos.top;
		height = height - footer.height();
		if (height > 0) {
			footer.css({
				'margin-top': height + 'px'
			});
		}else{
			footer.css({
				'margin-top': 20 + 'px'
			});
		}
		footer.addClass("ready");
	}
});
