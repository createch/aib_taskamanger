var User = Backbone.Model.extend({
	urlRoot: '/api/Members',
	/**
	 * get a list of tokens on the server for this user
	 * @param callback
	 */
	tokens: function (callback) {
		var self = this;
		if (!self.get("id")) {
			//no id = unAuthenticated, doesn't have tokens
			callback("unAuthenticated user");
			return;
		}
		var url = '/api/Members/' + this.get("id") + "/accessTokens";
		$.ajax({
			method: "GET",
			dataType: "json",
			url: url,
			success: function (data, textStatus, jqxhr) {
				callback(null, data);
			},
			error: function (jqxhr, textStatus, error) {
				callback(jqxhr.responseJSON.error, null);
			}
		});
	},
	/**
	 * Check if  credentials are valid (app_userId = MY_USER.id)
	 * If callback is provided make a query to the server and return error if
	 * the user is not authenticated.
	 */
	isAuthenticated: function (callback) {
		var app_userId = window.readCookie("app_userId");
		var logged = (app_userId && app_userId === this.get("id")+"");
		if(callback){
			// query the api to check that user has valid tokens
			this.tokens(function (err) {
				callback(err);
			});
		}
		// returns true if "app_userId" cookie is the same as the id of this user
		return logged;
	}
});

var UserCollection = Backbone.Collection.extend({
	url: '/api/Members',
	model: User
});


var LoginView = Backbone.View.extend({
	events: {
		'click .close': 'close',
		'submit #form-login': 'login'
	},
	$form: null,
	formData: null,
	initialize: function () {
		this._templateHTML = templates.login;
	},
	render: function (m) {
		if (!this.el.hasChildNodes()) {
			//avoid re-render if render() is called multiple times
			//initialize selectors
			this.$el.append(Mustache.render(this._templateHTML, m));
			this.$form = this.$el.find("#form-login");
		}
		this.$el.find("#login-again").hide();
		this.$el.find(".error").hide();
		return this;
	},
	renderLoginAgain: function () {
		this.$el.find("#login-again").show();
	},
	getFormData: function () {
		this.formData = transForm.serialize(this.$form.selector);
		return this.formData;
	},
	login: function (e) {
		if (e)
			e.preventDefault();
		var self = this;
		var d = this.getFormData();
		$.ajax({
			method: "POST",
			dataType: "json",
			url: '/api/Members/login',
			data: {email: d.email, password: d.password, rememberMe: d.rememberMe},
			success: function (data) {
				$(document).trigger("logged-in");
				var app_userId = window.readCookie("app_userId");
				MY_USER = new User({id: app_userId});
				MY_USER.fetch();
				if(d.successUrl){
					window.location.href = "/#"+d.successUrl;
					window.location.reload();
					//app.navigate(d.successUrl, {trigger: true});
				}

			},
			error: function (jqxhr) {
				self.$el.find(".error").slideDown();
			}
		});
	}
},{
	logout: function (callback) {
		$.ajax({
			method: "POST",
			dataType: "json",
			url: '/api/Members/logout',
			success: function (data, textStatus, jqxhr) {
				$(document).trigger("logged-out");
				callback();
			},
			error: function (jqxhr, textStatus, error) {
				callback(error);
			}
		});
	},
});


/**
 * Add login routes to the main APP
 */
$(document).on("AppReady", function () {
	app.views.login = new LoginView();
	app.route("login", "login", function () {
		app.addView("#content", "login");
	});
	app.route("logout", "logout", function () {
		LoginView.logout(function(err){
			if(err){
				alert("log-out error: " + error);
			}else{
				app.navigate("login", {trigger: true});
			}
		});
	});
});

/**
 * initializes the  MY_USER
 */
function init_MY_USER(callback){
	var app_userId = window.readCookie("app_userId");
	MY_USER = new User({id: app_userId});
	MY_USER.isAuthenticated(function(err){
		//auth == true if the user logged in is present
		if(err){
			createCookie('app_userId', '', 1);
			if (Backbone.History.started && APP) {
				// if backbone history estarted then navigate to login page
				app.navigate("login", {trigger: true});
			} else {
				// if backbone history bot estarted then change hash to login page
				// and login page will be displayed once
				window.location.hash = "login";
			}
		}else{
			MY_USER.fetch();
		}
		callback();
	});
}


