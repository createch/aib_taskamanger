/**
 * Created by marlon.jerez on 16/06/2015.
 */

var CalendarView = Backbone.View.extend({
	defaults: {},
	events: {
		'click .close': 'close',
		'click .bg': 'close'
	},
	initialize: function () {
		// bind the functions 'add' and 'remove' to the view.
		_(this).bindAll('add', 'remove', 'change');
		// init & bind this view to the agencies collection!
		this.collection.bind('add', this.add);
		this.collection.bind('remove', this.remove);
		this.collection.bind('change', this.change);

		//render existing elements
		this.$el.empty();
		this.collection.each(this.add);
	},
	render: function () {
		if (!this.el.hasChildNodes()) {
			this.$el.fullCalendar({
				header: {
					left: 'prev,next today ',
					center: 'title',
					right: 'month,basicWeek,basicDay'
				},
				eventClick: function (event, jsEvent, view) {
					var view = app.views.eventForm;
					if (!view)
						throw Error("app.views.eventForm not initialized");
					var event = app.models.Events.get(event.id);

					if (view.getEvent()) {
						if (event.id == view.getEvent().id)//same event displaying currently
							return;
					}
					view.setEvent(event);
					view.eventInfo();
				},
				dayClick: function (date, jsEvent, view) {
					var diff = moment().startOf("day").diff(date.startOf("day"));
					if (diff >= 0)
						return;
					var view = app.views.eventForm;
					if (!view)
						throw Error("app.views.eventForm not initialized");
					if (!view.eventView) {
						view.eventCreate(null, date, function () {
							view.resetDate();
						});
					} else if (view.eventView === "create") {
						return;
						view.$el.find("#from_date").val(date.format("DD/MM/YYYY"));
						view.resetDate();
					}
				},
				eventRender: function (event, element) {
					var cat = " <span class='fc-category' style='background-color:"+event.category_color+"'>&nbsp;</span>";
					var channel = " <span class='fc-channel' style='background-color:"+event.channel_color+"'>&nbsp;</span>";
					$(element).find(".fc-title").append("<span class='fc-event-cn'>"+cat+channel+"</span>");
				}
			});
		}
		return this;
	},
	prepareEvent: function (event) {
		event = event.clone();
		event.set("title", event.get("name"));
		event.set("start", event.get("from_date"));
		event.set("end", event.get("to_date"));
		event.set("className", "app_event agency_" + event.get("agencyId") + " category_" + slugify(event.get("categoryId")) + " channel_" + event.get("socialChannelId"));
		event.set("category_color",app.models.Categories.get(event.get("categoryId")).get("color"));
		event.set("channel_color",app.models.Channels.get(event.get("socialChannelId")).get("color"));
		var agencyId = event.get("agencyId");
		var agency = app.models.Agencies.get(agencyId);
		if (agency) {
			event.set("color", agency.get("color"));
		}
		return event.toJSON();
	},
	add: function (event) {
		// add item to the calendar
		var json = this.prepareEvent(event);
		this.$el.fullCalendar('renderEvent', json, true);
	},
	remove: function (event) {
		//this.$el.find("#filter_" + agency.get("name")).remove();
		this.$el.fullCalendar('removeEvents', event.get("id"));
	},
	updateEvent: function (event, fc_event) {
		fc_event.start = event.get("from_date");
		fc_event.end = event.get("to_date");
		fc_event.className = "app_event agency_" + event.get("agencyId") + " category_" + event.get("categoryId") + " channel_" + event.get("socialChannelId");
		fc_event.category_color = app.models.Categories.get(event.get("categoryId")).get("color");
		fc_event.channel_color = app.models.Channels.get(event.get("socialChannelId")).get("color");
		var agencyId = event.get("agencyId");
		var agency = app.models.Agencies.get(agencyId);
		if (agency) {
			fc_event.color = agency.get("color");
		}
		return fc_event;
	},
	change: function (event) {
		// update an event in the calendar
		var fc_event = this.$el.fullCalendar('clientEvents', event.get("id"));
		if (fc_event.length == 1)
			this.$el.fullCalendar('updateEvent', this.updateEvent(event, fc_event[0]));
	}
});


