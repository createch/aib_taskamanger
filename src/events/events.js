/**
 * Created by marlon.jerez on 20/05/2015.
 */
var Event = Backbone.Model.extend({
	urlRoot: '/api/Tasks'
});
var EventsCollection = Backbone.Collection.extend({
	url: '/api/Tasks',
	model: Event
});

var EventFormView = Backbone.View.extend({
	events: {
		'click #categorySelect .c_item': 'categorySelected',
		'click #channelSelect .c_item': 'channelSelected',
		'click .reset_form': "reset",
		'click .eventCreate': "eventCreate",
		'click .eventEdit': "eventEdit",
		'click .eventInfo': "eventInfo",
		'click .eventDelete': "eventDelete",
		'click .eventCancel': "eventCancel",
		'click .cancel': "renderInfoMessage",
		'submit #taskForm': 'confirm',
		"change #from_date": "resetDate"
	},
	titles: {
		create: "Create Event &nbsp;<i class='fa fa-calendar'></i>",
		edit: "Edit Event  &nbsp;<i class='fa fa-info-circle'></i>",
		info: "Event Info  &nbsp;<i class='fa fa-info-circle'></i>"
	},
	initialize: function () {
		this._templateHTML = templates.eventForm;
		this._InfoTemplate = templates.infoEvents;
		this.disable = true;
	},
	setEvent: function (model) {
		this.model = model;
		this.modelAttribs = model.attributes;
	},
	getEvent: function () {
		return this.modelAttribs;
	},
	reset: function (e, from) {
		this.disabled = false;
		this.render(from);
	},
	eventCreate: function (e, from, callback) {
		this.eventView = "create";
		this.model = null;
		this.modelAttribs = null;
		this.disabled = false;
		this.fadeRender(from, callback);
	},
	eventEdit: function (e, data) {
		this.eventView = "edit";
		this.disabled = false;
		this.render(data);
	},
	eventInfo: function (e, data, callback) {
		this.eventView = "info";
		this.disabled = true;
		this.fadeRender(data, callback);
	},
	eventDelete: function (e, data) {
		this.eventView = "delete";
		this.confirm(e);
	},
	eventCancel: function (e) {
		this.eventView = "cancel";
		this.renderInfoMessage();
	},
	renderInfoMessage: function (data) {
		this.model = null;
		this.eventView = null;
		this.modelAttribs = null;
		this.disabled = true;
		this.$el.empty();
		this.$el.append(Mustache.render(this._InfoTemplate, data));
		return this;
	},
	render: function (from) {
		var self = this;
		this.$el.empty();
		var data = this.getRenderData(from);
		this.$el.append(Mustache.render(this._templateHTML, data));
		this.enableControls();
		return this;
	},
	fadeRender: function (data, callback) {
		var self = this;

		var fadeDisabled = false;
		if (fadeDisabled) {
			self.render(data);
			if (self.eventView == "create") self.$el.find("#name").focus();
			if (callback) callback();
		} else {
			this.$el.fadeOut(300, function () {
				self.render(data);
				self.$el.fadeIn(300, function () {
					if (self.eventView == "create") self.$el.find("#name").focus();
					if (callback) callback();
				});
			});
		}
	},
	getRenderData: function (from) {
		var data = {};
		data.disabled = this.disabled;
		data.categories = app.models.Categories.models;
		data.channels = app.models.Channels.models;
		if (this.eventView == "create") {
			data.title = this.titles.create;
			data.created_on = Date.now();
			data.creatorId = MY_USER.get("id");
			data.createControls = true;
			if (from)
				data.FROM = moment(from).format("DD/MM/YYYY");
		} else {
			data.title = (this.disabled) ? this.titles.info : this.titles.create;
			data.created_on = this.modelAttribs.created_on;
			data.creatorId = this.modelAttribs.creatorId;
			data.event = this.modelAttribs;
			data.FROM = moment(this.modelAttribs.from_date).format("DD/MM/YYYY");
			data.TO = moment(this.modelAttribs.to_date).format("DD/MM/YYYY");
			data.categoryName = app.models.Categories.get(this.modelAttribs.categoryId).get("name");
			data.socialChannelName = app.models.Channels.get(this.modelAttribs.socialChannelId).get("name");
		}
		if (this.eventView == "info")
			data.editControls = (this.modelAttribs) ? this.modelAttribs.agencyId == MY_USER.get("agencyId") : false;

		return data;
	},
	enableControls: function () {
		if (!this.disabled) {
			//this.categoriesF = new FilterView({
			//	el: this.$el.find("#categorySelect")[0],
			//	collection: app.models.Categories,
			//	selectMode: true
			//});
			//this.cahnnelF = new FilterView({
			//	el: this.$el.find("#channelSelect")[0],
			//	collection: app.models.Channels,
			//	selectMode: true
			//});
			if(this.model){
				this.$el.find("#socialChannelId option[value='"+this.model.get("socialChannelId")+"']").prop("selected",true);
				this.$el.find("#categoryId option[value='"+this.model.get("categoryId")+"']").prop("selected",true);
			}

			var now = moment();
			this.$el.find("#from_date").daterangepicker({
				format: 'DD/MM/YYYY',
				startDate: now,
				minDate: now,
				singleDatePicker: true,
				showDropdowns: true
			});
			this.$el.find("#to_date").daterangepicker({
				format: 'DD/MM/YYYY',
				startDate: now,
				minDate: now,
				singleDatePicker: true,
				showDropdowns: true
			});
		}
	},
	animateFrom: function ($from, $to) {
		$to.focus();
		$from.addClass("transition");
		window.setTimeout(function () {
			$from.removeClass("transition");
		}, 200);
	},
	resetDate: function (e) {
		var $from = this.$el.find("#from_date");
		var $to = this.$el.find("#to_date");
		//this.animateFrom($from,$to);
		var min = moment($from.val(), 'DD/MM/YYYY');
		$to.val("");
		$to.daterangepicker({
			format: 'DD/MM/YYYY',
			startDate: min,
			minDate: min,
			singleDatePicker: true,
			showDropdowns: true
		});
	},
	getFormData: function () {
		this.formData = transForm.serialize("#taskForm",{skipDisabled:false});
		var data = this.formData;
		data.categoryId = parseInt(data.categoryId,10);
		data.categoryName = app.models.Categories.get(data.categoryId).get("name");
		data.socialChannelId = parseInt(data.socialChannelId,10);
		data.socialChannelName = app.models.Channels.get(data.socialChannelId).get("name");
		data.memberId = parseInt(data.memberId,10);
		data.from_date = moment(data.from_date, 'DD/MM/YYYY').startOf('day').add(1, 'h').toDate();
		data.to_date = moment(data.to_date, 'DD/MM/YYYY').startOf('day').add(23, 'h').toDate();
		data.agencyId = MY_USER.get("agencyId");
		return this.formData;
	},
	channelSelected: function (e) {
		var $elem = $(e.currentTarget);
		$elem.attr("style", "");
		this.$el.find("#socialChannelId").val($elem.data("name"));
	},
	categorySelected: function (e) {
		var $elem = $(e.currentTarget);
		$elem.attr("style", "");
		var color = $elem.data("color");
		this.$el.find(".taskForm").css({
			"border-color": color
		});
		this.$el.find(".submit").css({
			"background-color": color
		});
		this.$el.find("#categoryId").val($elem.data("name"));
	},
	confirm: function (e) {
		e.preventDefault();
		var self = this;
		if (this.eventView == "create")
			var title = "<span class='text-primary'>Create Event</span>";
		if (this.eventView == "edit")
			var title = "<span class='text-primary'>Edit Event</span>";
		if (this.eventView == "delete")
			var title = "<span class='test-primary'>Delete Event</span>";

		var data = this.getFormData();
		//data.category = app.models.Agencies.get(data.categoryId).get("name");
		var content = Mustache.render(templates.eventConfirm, data);
		app.views.modal.showDialog(title, content, {
			confirm: function () {
				self.submit();
			},
			cancel: function () {
			}
		})
	},
	submit: function (e) {
		var self = this;
		var callbacks = {
			wait: true,    // waits for server to respond with 200 before adding newly created model to collection
			success: function (resp) {
				self.renderInfoMessage();
			},
			error: function (err) {
				self.renderInfoMessage();
				// this error message for dev only
				alert('There was an error on the server, please try later.');
			}
		};
		if (!MY_USER || !MY_USER.get("agencyId")) {
			alert("aplication error: MY_USER.agencyId not defined ");
			return;
		}

		if (this.model && this.eventView == "delete") {
			this.model.destroy(callbacks);
			return;
		}

		var data = this.getFormData();
		if (app.models.Events && this.eventView === "create") {
			data.created_on = new Date(Date.now());
			app.models.Events.create(data, callbacks);
		} else if (this.model && this.eventView == "edit") {
			this.model.save(data, callbacks);
		}
	}
});




