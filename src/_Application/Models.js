/**
 * Created by marlon.jerez on 19/05/2015.
 */
var Agency = Backbone.Model.extend({
	urlRoot: '/api/Agencies'
});
var AgenciesCollection = Backbone.Collection.extend({
	url: '/api/Agencies',
	model: Agency
});

var Category = Backbone.Model.extend({
	urlRoot: '/api/Categories'
});
var CategoriesCollection = Backbone.Collection.extend({
	url: '/api/Categories',
	model: Category
});

var Channel = Backbone.Model.extend({
	urlRoot: '/api/SocialChannels'
});
var ChannelsCollection = Backbone.Collection.extend({
	url: '/api/SocialChannels',
	model: Channel
});





