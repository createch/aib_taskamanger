/**
 * Created by marlon.jerez on 19/05/2015.
 */
var PageView = Backbone.View.extend({
	setPage: function (pageName) {
		if (templates[pageName]) {
			this._templateHTML = templates[pageName];
		} else {
			this._templateHTML = templates.error404;
		}
	},
	render: function (m) {
		this.$el.empty();
		this.$el.append(Mustache.render(this._templateHTML, m));
		return this;
	}
});



