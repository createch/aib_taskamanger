var Application = Backbone.Router.extend({

	routes: {
		"": "start",
		"page/:pageName": "pageView",
		"*notFound": "notFound"
	},
	models: {},
	views: {
		nav : null,
		footer : null,
		pageView : null,
		modal:null
	},
	//stores a referenc all al the views added to one location of the page
	locations:{},
	initialize: function () {
		var self = this;
		self.views.nav = new NavView();
		self.views.footer = new FooterView();
		self.views.pageView = new PageView();
		self.views.modal = new ModalView();
	},

	start: function () {
		this.pageView("index");
	},


	pageView: function(pageName){
		this.views.pageView.setPage(pageName);
		this.addView("#content","pageView");
	},


	notFound:function(){
		this.pageView("error404");
	},


	_GET: function (variable) {
		var vars = {};
		var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function (m, key, value) {
			vars[key] = value;
		});
		return vars[variable];
	},


	// add an existing View to a Location on the current page
	// if "append" == false all the views a detached from the especified location;
	addView: function (locationSelector, viewName, append) {
		var self = this;
		var location = (this.locations[locationSelector])?this.locations[locationSelector]:{};
		var view = this.views[viewName];
		var pageElem = $(locationSelector);

		//check if viewName & location  exist
		if(!this.views[viewName])
			throw new Error("Non existing view: ",viewName);
		if(!pageElem.length)
			throw new Error("Non existing Location: ",locationSelector);


		//clear the existing views in the specified location
		if(!append){
			// calls the deRender() method from every view on the current location
			$.each(location,function(i,view){
				view.$el.detach();
				if(typeof(view.deRender) === "function")
					view.deRender();
			});
			// clars all teh view fro the location
			location = {};
		}

		// append the view to the location and then render it
		pageElem.append(view.$el);
		if(typeof(view.render) === "function")
			view.render();

		// update this.locations;
		location[viewName] = view;
		this.locations[locationSelector] = location;

		// update footer position
		window.setTimeout(function () {
			self.views.footer.position();
		},200);
	},

	//check if the user is authenticated if not it is redirected to the login view
	addPrivateView: function (locationSelector, viewName, append) {
		if(MY_USER && MY_USER.isAuthenticated()){
			this.addView(locationSelector, viewName, append);
		}else{
			this.navigate("login", {trigger: true});
		}
	},

	clear: function () {
		config.contentArea.html('');
	},

	log: function (str) {
		console.log(str);
	}
});



