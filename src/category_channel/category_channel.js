CategoryCahnnelFormView = Backbone.View.extend({
	events: {
		'submit #categoryForm': 'submit',
		'click .submit': 'validate',
		'click .confirm': 'confirm',
		'click .cancel': 'cancel'
	},
	//to ensure we use use a dark color on the color piker, max = 255 * 3; min = 0;use rgb colors
	maxClear : 385,
	// to ensure the random generated color is no so dark
	maxDark:150,
	initialize: function (options) {
		this._templateHTML = templates.category_channelForm;
		if(options.modelName === "channel"){
			this.collection = app.models.Channels;
			this.modelName = "Channel";
		}else{
			this.collection = app.models.Categories;
			this.modelName = "Campaign";
		}
	},
	render: function () {
		var self = this;
		if (!this.el.hasChildNodes()) {
			//syncs collection with server the first time when we open the modal
			this.collection.fetch();
		}

		if (this.model) {
			this.$el.hide(300,function(){
				self.$el.empty();
				var x = Mustache.render(self._templateHTML, {model:self.model,modelName:self.modelName});
				self.$el.append(x);
				self.$el.show(300);
			});
		}else{
			this.$el.empty();
			this.$el.append(Mustache.render(this._templateHTML,{modelName:this.modelName}));
			this.colorPiker = this.$el.find("#color");

			var total = 255 * 3;
			var a,b,c;
			var x =0;
			var maxDark = 100;
			while(total > this.maxClear || total < this.maxDark){
				//add + 1 to avoid get
				a = Math.floor(Math.random()*200)+1;
				b = Math.floor(Math.random()*200)+1;
				c = Math.floor(Math.random()*200)+1;
				total = a + b + c;
				console.log("color total:"+ total);
				console.log("color loop :"+ ++x);

			}
			var r = toHex(a);
			var g = toHex(b);
			var b = toHex(c);
			var color = '#'+r+g+b;
			console.log("color:#"+r+"-"+g+"-"+b);

			this.colorPiker.spectrum({
				color: color
			});
		}
		return this;
	},
	validate: function (e) {
		//e.preventDefault();
		var self = this;

		//Validate unique category
		var name = encodeURIComponent(this.$el.find("#name").val().toLowerCase());
		var cat = this.collection.findWhere({name:name});
		if (cat) {
			//if category exist then throw a validation error
			self.$el.find("#name").setCustomValidity("Please select a different name.");
		} else {
			self.$el.find("#name").setCustomValidity("");
		}


		//validate color is dark
		self.$el.find("#color").attr("style", "display:block");
		var color = this.colorPiker.spectrum('get').toHexString().toUpperCase();
		var r = parseInt(color.substring(1, 3), 16);
		var g = parseInt(color.substring(3, 5), 16);
		var b = parseInt(color.substring(5, 7), 16);
		var sum = r + g + b;
		if (sum >= this.maxClear) {
			self.$el.find("#color").setCustomValidity("This color is not dark enough.");
		} else {
			self.$el.find("#color").setCustomValidity("");
		}


	},
	submit: function (e) {
		e.preventDefault();
		this.model = {
			name: encodeURIComponent(this.$el.find("#name").val().toLowerCase()),
			color: this.colorPiker.spectrum('get').toHexString().toUpperCase()
		};
		this.render();
	},
	confirm: function () {
		this.collection.create(this.model,{
			wait : true,
			error: function (err) {
				console.log(err);
				alert('There was an error on the server, please try later.');
			}
		});
	},
	cancel: function () {
		var  m = this.model;
		this.model = null;
		this.render();
		this.$el.find("#name").val(decodeURIComponent(m.name));
		this.colorPiker.spectrum({
			color: m.color
		});
	}

});
function toHex(int){
	var x = int.toString(16);
	if(x.length < 2)
		x = "0"+x;
	return x;
}
