var NavView = Backbone.View.extend({
	el: '#PageNav',
	defaults: {
	},
	events: {
		'click .close': 'close',
		'click .bg': 'close'
	},
	initialize: function() {
		this._templateHTML = templates.modal;
		this.render();
	}
});
