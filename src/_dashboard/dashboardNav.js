/**
 * Created by marlon.jerez on 17/06/2015.
 */
var DashNavView = Backbone.View.extend({
	id: "dashboardNav",
	tagName: "ul",
	className: "nav navbar-nav navbar-right",
	events: {
		"click #newCategory": "newCategory",
		"click #newChannel": "newChannel"
	},
	initialize: function () {
		this._templateHTML = templates.dashboardNav;
	},
	render: function (data) {
		if (!this.el.hasChildNodes()) {
			this.$el.append(Mustache.render(this._templateHTML, data));
			this.$el.find(".collapse").collapse();
		}
		return this;
	},
	newCategory: function (e) {
		e.preventDefault();
		app.views.modal.setSize("small");
		app.views.modal.setCentered(true);
		app.views.modal.showView(null, new CategoryCahnnelFormView({
			collection:app.models.Categories,
			modelName:"category"
		}));
	},
	newChannel: function (e) {
		e.preventDefault();
		app.views.modal.setSize("small");
		app.views.modal.setCentered(true);
		app.views.modal.showView(null, new CategoryCahnnelFormView({
			collection:app.models.Channels,
			modelName:"channel"
		}));
	}
});

$(document).on("logged-out", function () {
	app.views.dashboardNav.$el.empty();
});
