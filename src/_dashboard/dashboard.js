var DashboardView = Backbone.View.extend({
	initialize: function () {
		this._templateHTML = templates.dashboard;

	},
	render: function (data) {
		var self = this;
		if (!this.el.hasChildNodes()) {
			this.$el.append(Mustache.render(this._templateHTML, data));
			// init calendar & side form
			app.views.calendar = new CalendarView({el: this.$el.find("#calendar")[0], collection: app.models.Events}).render();
			app.views.eventForm = new EventFormView({el: this.$el.find("#calendar_side")[0]}).renderInfoMessage();

			// init section filter
			this.agenciesF = new FilterView({
				el: this.$el.find("#agenciesFilter")[0],
				collection: app.models.Agencies,
				type: "agency"
			});
			this.categoriesF = new FilterView({
				el: this.$el.find("#categoryFilter")[0],
				collection: app.models.Categories,
				type: "category"
			});
			this.cahnnelF = new FilterView({
				el: this.$el.find("#channelFilter")[0],
				collection: app.models.Channels,
				type: "channel"
			});
		}
		return this;
	},
	setNavView: function (navView) {
		this.navView = navView;
	},
	deRender: function () {
		if (this.navView)
			this.navView.$el.detach();
	}
});

// intialize dashboard
$(document).on("AppReady", function () {
	//init the collections
	app.models.Agencies = new AgenciesCollection();
	app.models.Categories = new CategoriesCollection();
	app.models.Channels = new ChannelsCollection();
	app.models.Events = new EventsCollection();
	app.views.dashboard = new DashboardView();
	app.views.dashboardNav = new DashNavView();
	app.views.dashboard.setNavView(app.views.dashboardNav);

	app.route("dashboard", "dashboard", function () {
		if(MY_USER.isAuthenticated()){
			var queries = 3;
			function dataLoaded(){
				queries --;
				if(queries === 0){
					//inint dashboard view
					app.addPrivateView("#content", "dashboard");
					app.addPrivateView("#navbar-content", "dashboardNav", true);
					app.models.Events.fetch().complete(dataLoaded);
				}
			}
			app.models.Agencies.fetch().complete(dataLoaded);
			app.models.Categories.fetch().complete(dataLoaded);
			app.models.Channels.fetch().complete(dataLoaded);
		}else{
			app.navigate("login", {trigger: true});
		}
	});
});

