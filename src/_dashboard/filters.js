/**
 * Created by marlon.jerez on 17/06/2015.
 */
var FilterView = Backbone.View.extend({
	events: {
		"click .filter": "filter",
		"click .select": "select"
	},
	_templateHTML: '<span class="label c_item filter active" data-name="{{name}}" data-id="{{id}}" ' +
	'data-color="{{color}}" style="background-color:{{color}}">{{name}}</span> ',
	initialize: function (options) {
		this.type = options.type || "";
		if (options.selectMode)
			this._templateHTML = this._templateHTML.replace("filter", "select");
		// bind the functions 'add' and 'remove' to the view.
		_(this).bindAll('add', 'remove');
		// init & bind this view to the agencies collection!
		this.collection.bind('add', this.add);
		this.collection.bind('remove', this.remove);
		//render existing elements
		this.$el.empty();
		this.collection.each(this.add);
	},
	add: function (item) {
		// add item to the view
		this.$el.append(Mustache.render(this._templateHTML, item.attributes));
	},
	remove: function (item) {
		this.$el.find("#filter_" + item.get("name")).remove();
	},
	filter: function (e) {
		var $elem = $(e.currentTarget);
		var id = ($elem.data("id")) ? $elem.data("id") : $elem.data("name");
		var item = this.collection.get(id);

		var name = item.get("name");
		var color = item.get("color");
		var filterSelector = ".app_event." + this.type + "_" + slugify(id);
		//console.log(filterSelector);
		$elem.toggleClass("active");
		if ($elem.hasClass("active")) {
			$elem.attr("style", "background-color:" + color);
			$(filterSelector).removeClass("hide_event");
		} else {
			$elem.attr("style", "");
			$(filterSelector).addClass("hide_event");
		}
	},
	select: function (e) {
		var $elem = $(e.currentTarget);
		$elem.addClass("selected");
		this.$el.find(".c_item:not(.selected)").remove();
	}
});
