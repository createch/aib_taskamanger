/**
 * Created by Ma jerez on 24/05/2015.
 */
/**
 * Open a modal window
 * To open a modal just add the class .modal-trigger to any element.
 * The content of the modal window is automatically loaded reading the data attribute in four different ways:
 * 1 - data-template="templateName" => load one templates from templates.json.
 * 2 - data-url="http://abcd...."  => load the content from an url.
 * 3 - data-message="Hello this is the message" => load the modal and display the message.
 * 4 - data-selector="#ModalContent" => clone one element using the selector and call show() to display the
 *    element if it is hidden.
 *
 */
var ModalView = Backbone.View.extend({
	$modal: null,
	$content: null,
	centered: false,
	initialize: function () {
		$("body").append(templates.modal);
		this.$modal = $("#Modal");
		this.$content = this.$modal.find(".modal-content");
		_.bindAll(this, 'moodalTrigger','dialogConfirm','dialogCancel' , "modalClose");
		$(document).on("click", ".modal-trigger", this.moodalTrigger);
		$(document).on("click", "#Modal .dialogConfirm", this.dialogConfirm);
		$(document).on("click", "#Modal .dialogCancel", this.dialogCancel);
		this.$modal.on("hidden.bs.modal",this.modalClose);
		this._modalMessage = templates.modalMessage;
	},
	show: function () {
		var self = this;
		this.center();
		this.$modal.modal("show");
		if (this.close_timeout >= 0) {
			window.setTimeout(function () {
				self.$modal.modal('hide');
			}, this.close_timeout)
		}
	},
	setSize: function (size) {
		if (size === "large") {
			this.$modal.find(">.modal-dialog").attr("class", "modal-dialog modal-lg");
		} else if (size === "small") {
			this.$modal.find(">.modal-dialog").attr("class", "modal-dialog modal-sm");
		} else {
			this.$modal.find(">.modal-dialog").attr("class", "modal-dialog");
		}
	},
	setCentered: function (centered) {
		this.centered = centered;
	},
	modalClose: function (e) {
		this.setCentered(false);
		this.setSize("medium");
	},
	setCloseTimeout: function (time) {
		this.close_timeout = parseInt(time);
	},
	// this must be called after the content has been appended to this.$content
	center: function () {
		if (this.centered) {
			var $clone = this.$modal.clone().css('display', 'block').appendTo('body');
			var top = Math.round(($clone.height() - $clone.find('.modal-content').height()) / 2);
			top = top > 0 ? top : 0;
			$clone.remove();
			this.$content.css("margin-top", top);
		} else {
			this.$content.css("margin-top", 0);
		}
	},
	/**
	 * Automatically shows a modal window loading the conten
	 */
	moodalTrigger: function (e) {
		e.preventDefault();
		this.$content.empty();
		this.close_timeout = 0;
		var $elem = $(e.currentTarget);

		// content load method
		var template = $elem.data("template");
		var url = $elem.data("url");
		var message = $elem.data("message");
		var title = $elem.data("title");
		var selector = $elem.data("selector");

		//config parameters
		var size = $elem.data("size");
		var close_timeout = $elem.data("close-timeout");
		var centered = $elem.data("centered");
		this.setCentered(centered);
		this.setSize(size);
		this.setCloseTimeout(close_timeout);

		//select load methid
		if (template)
			this.showTemplate(template);
		else if (url)
			this.showURL(url);
		else if (message)
			this.showMessage( title,message);
		else if (selector)
			this.showDOMelement(selector);
		else
			this.showTriggerError();

	},
	/**
	 * Shows a view on a modal window.
	 * it call view.render() and then append view.$el to the modal window
	 */
	showView: function (data, view) {
		this.$content.empty();
		view.render(data);
		this.$content.append(view.$el);
		this.show();
	},
	showTemplate: function (templateName) {
		this.$content.empty();
		if (templates[templateName]) {
			this.$content.append(templates[templateName]);
		} else {
			this.$content.append(templates["modalError"]);
		}
		this.$modal.modal('show');
	},
	showURL: function (url) {
		this.$content.empty();
		$.ajax({
			url: url,
			success: function (data) {
				this.$content.append(data);
				this.show();
			},
			error: function () {
				this.$content.append(templates["modalError"]);
				this.show();
			}
		});
	},
	showMessage: function (title, message) {
		this.$content.empty();
		this.$content.append(Mustache.render(this._modalMessage, {message: message, title: title}));
		this.show();
	},
	showDialog: function (title ,message, callbacks) {
		if(callbacks){
			this.confirmCallback = callbacks.confirm;
			this.cancelCallback = callbacks.cancel;
		}
		this.$content.empty();
		this.$content.append(Mustache.render(this._modalMessage, {message: message, title: title,dialog:true}));
		this.show();
	},
	showDOMelement: function (selector) {
		this.$content.empty();
		var content = $(selector);
		if (content.length) {
			this.$content.append(content.clone().show());
		} else {
			this.$content.append(templates["modalError"]);
		}
		this.show();
	},
	showTriggerError: function () {
		// wrong modal trigger configuration
		this.setSize("normal");
		this.setCentered(true);
		this.setCloseTimeout(3000);

		var title = "Modal button not configured properly";
		var message =
			"<strong>data-url:</strong> loads the contem fron and ajax request.<br/>" +
			"<strong>data-template</strong> loads the contem from /templates/templates.json.<br/>" +
			"<strong>data-selector</strong> loads the contem froma an elemnt on the dom ie: '#MY-modal-conten'.<br/>" +
			"<strong>data-message.</strong> print the value of the attributes data-message and data-title (optional).<br/>";
		this.showMessage(message, title);
	},
	dialogConfirm:function(e){
		if(this.confirmCallback && typeof this.confirmCallback === 'function')
			this.confirmCallback(e);
	},
	dialogCancel: function (e) {
		if(this.cancelCallback && typeof this.cancelCallback === 'function')
			this.cancelCallback(e);
	}

});
