var Application = Backbone.Router.extend({

	routes: {
		"": "start",
		"page/:pageName": "pageView",
		"*notFound": "notFound"
	},
	models: {},
	views: {
		nav : null,
		footer : null,
		pageView : null,
		modal:null
	},
	//stores a referenc all al the views added to one location of the page
	locations:{},
	initialize: function () {
		var self = this;
		self.views.nav = new NavView();
		self.views.footer = new FooterView();
		self.views.pageView = new PageView();
		self.views.modal = new ModalView();
	},

	start: function () {
		this.pageView("index");
	},


	pageView: function(pageName){
		this.views.pageView.setPage(pageName);
		this.addView("#content","pageView");
	},


	notFound:function(){
		this.pageView("error404");
	},


	_GET: function (variable) {
		var vars = {};
		var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function (m, key, value) {
			vars[key] = value;
		});
		return vars[variable];
	},


	// add an existing View to a Location on the current page
	// if "append" == false all the views a detached from the especified location;
	addView: function (locationSelector, viewName, append) {
		var self = this;
		var location = (this.locations[locationSelector])?this.locations[locationSelector]:{};
		var view = this.views[viewName];
		var pageElem = $(locationSelector);

		//check if viewName & location  exist
		if(!this.views[viewName])
			throw new Error("Non existing view: ",viewName);
		if(!pageElem.length)
			throw new Error("Non existing Location: ",locationSelector);


		//clear the existing views in the specified location
		if(!append){
			// calls the deRender() method from every view on the current location
			$.each(location,function(i,view){
				view.$el.detach();
				if(typeof(view.deRender) === "function")
					view.deRender();
			});
			// clars all teh view fro the location
			location = {};
		}

		// append the view to the location and then render it
		pageElem.append(view.$el);
		if(typeof(view.render) === "function")
			view.render();

		// update this.locations;
		location[viewName] = view;
		this.locations[locationSelector] = location;

		// update footer position
		window.setTimeout(function () {
			self.views.footer.position();
		},200);
	},

	//check if the user is authenticated if not it is redirected to the login view
	addPrivateView: function (locationSelector, viewName, append) {
		if(MY_USER && MY_USER.isAuthenticated()){
			this.addView(locationSelector, viewName, append);
		}else{
			this.navigate("login", {trigger: true});
		}
	},

	clear: function () {
		config.contentArea.html('');
	},

	log: function (str) {
		console.log(str);
	}
});




/**
 * Created by marlon.jerez on 19/05/2015.
 */
var Agency = Backbone.Model.extend({
	urlRoot: '/api/Agencies'
});
var AgenciesCollection = Backbone.Collection.extend({
	url: '/api/Agencies',
	model: Agency
});

var Category = Backbone.Model.extend({
	urlRoot: '/api/Categories'
});
var CategoriesCollection = Backbone.Collection.extend({
	url: '/api/Categories',
	model: Category
});

var Channel = Backbone.Model.extend({
	urlRoot: '/api/SocialChannels'
});
var ChannelsCollection = Backbone.Collection.extend({
	url: '/api/SocialChannels',
	model: Channel
});






/**
 * Created by marlon.jerez on 19/05/2015.
 */
var PageView = Backbone.View.extend({
	setPage: function (pageName) {
		if (templates[pageName]) {
			this._templateHTML = templates[pageName];
		} else {
			this._templateHTML = templates.error404;
		}
	},
	render: function (m) {
		this.$el.empty();
		this.$el.append(Mustache.render(this._templateHTML, m));
		return this;
	}
});




/**
 * Created by marlon.jerez on 02/06/2015.
 */
function slugify(text)
{
	return text.toString().toLowerCase()
		.replace(/\s+/g, '-')           // Replace spaces with -
		.replace(/[^\w\-]+/g, '')       // Remove all non-word chars
		.replace(/\-\-+/g, '-')         // Replace multiple - with single -
		.replace(/^-+/, '')             // Trim - from start of text
		.replace(/-+$/, '');            // Trim - from end of text
}

var DashboardView = Backbone.View.extend({
	initialize: function () {
		this._templateHTML = templates.dashboard;

	},
	render: function (data) {
		var self = this;
		if (!this.el.hasChildNodes()) {
			this.$el.append(Mustache.render(this._templateHTML, data));
			// init calendar & side form
			app.views.calendar = new CalendarView({el: this.$el.find("#calendar")[0], collection: app.models.Events}).render();
			app.views.eventForm = new EventFormView({el: this.$el.find("#calendar_side")[0]}).renderInfoMessage();

			// init section filter
			this.agenciesF = new FilterView({
				el: this.$el.find("#agenciesFilter")[0],
				collection: app.models.Agencies,
				type: "agency"
			});
			this.categoriesF = new FilterView({
				el: this.$el.find("#categoryFilter")[0],
				collection: app.models.Categories,
				type: "category"
			});
			this.cahnnelF = new FilterView({
				el: this.$el.find("#channelFilter")[0],
				collection: app.models.Channels,
				type: "channel"
			});
		}
		return this;
	},
	setNavView: function (navView) {
		this.navView = navView;
	},
	deRender: function () {
		if (this.navView)
			this.navView.$el.detach();
	}
});

// intialize dashboard
$(document).on("AppReady", function () {
	//init the collections
	app.models.Agencies = new AgenciesCollection();
	app.models.Categories = new CategoriesCollection();
	app.models.Channels = new ChannelsCollection();
	app.models.Events = new EventsCollection();
	app.views.dashboard = new DashboardView();
	app.views.dashboardNav = new DashNavView();
	app.views.dashboard.setNavView(app.views.dashboardNav);

	app.route("dashboard", "dashboard", function () {
		if(MY_USER.isAuthenticated()){
			var queries = 3;
			function dataLoaded(){
				queries --;
				if(queries === 0){
					//inint dashboard view
					app.addPrivateView("#content", "dashboard");
					app.addPrivateView("#navbar-content", "dashboardNav", true);
					app.models.Events.fetch().complete(dataLoaded);
				}
			}
			app.models.Agencies.fetch().complete(dataLoaded);
			app.models.Categories.fetch().complete(dataLoaded);
			app.models.Channels.fetch().complete(dataLoaded);
		}else{
			app.navigate("login", {trigger: true});
		}
	});
});


/**
 * Created by marlon.jerez on 17/06/2015.
 */
var DashNavView = Backbone.View.extend({
	id: "dashboardNav",
	tagName: "ul",
	className: "nav navbar-nav navbar-right",
	events: {
		"click #newCategory": "newCategory",
		"click #newChannel": "newChannel"
	},
	initialize: function () {
		this._templateHTML = templates.dashboardNav;
	},
	render: function (data) {
		if (!this.el.hasChildNodes()) {
			this.$el.append(Mustache.render(this._templateHTML, data));
			this.$el.find(".collapse").collapse();
		}
		return this;
	},
	newCategory: function (e) {
		e.preventDefault();
		app.views.modal.setSize("small");
		app.views.modal.setCentered(true);
		app.views.modal.showView(null, new CategoryCahnnelFormView({
			collection:app.models.Categories,
			modelName:"category"
		}));
	},
	newChannel: function (e) {
		e.preventDefault();
		app.views.modal.setSize("small");
		app.views.modal.setCentered(true);
		app.views.modal.showView(null, new CategoryCahnnelFormView({
			collection:app.models.Channels,
			modelName:"channel"
		}));
	}
});

$(document).on("logged-out", function () {
	app.views.dashboardNav.$el.empty();
});

/**
 * Created by marlon.jerez on 17/06/2015.
 */
var FilterView = Backbone.View.extend({
	events: {
		"click .filter": "filter",
		"click .select": "select"
	},
	_templateHTML: '<span class="label c_item filter active" data-name="{{name}}" data-id="{{id}}" ' +
	'data-color="{{color}}" style="background-color:{{color}}">{{name}}</span> ',
	initialize: function (options) {
		this.type = options.type || "";
		if (options.selectMode)
			this._templateHTML = this._templateHTML.replace("filter", "select");
		// bind the functions 'add' and 'remove' to the view.
		_(this).bindAll('add', 'remove');
		// init & bind this view to the agencies collection!
		this.collection.bind('add', this.add);
		this.collection.bind('remove', this.remove);
		//render existing elements
		this.$el.empty();
		this.collection.each(this.add);
	},
	add: function (item) {
		// add item to the view
		this.$el.append(Mustache.render(this._templateHTML, item.attributes));
	},
	remove: function (item) {
		this.$el.find("#filter_" + item.get("name")).remove();
	},
	filter: function (e) {
		var $elem = $(e.currentTarget);
		var id = ($elem.data("id")) ? $elem.data("id") : $elem.data("name");
		var item = this.collection.get(id);

		var name = item.get("name");
		var color = item.get("color");
		var filterSelector = ".app_event." + this.type + "_" + slugify(id);
		//console.log(filterSelector);
		$elem.toggleClass("active");
		if ($elem.hasClass("active")) {
			$elem.attr("style", "background-color:" + color);
			$(filterSelector).removeClass("hide_event");
		} else {
			$elem.attr("style", "");
			$(filterSelector).addClass("hide_event");
		}
	},
	select: function (e) {
		var $elem = $(e.currentTarget);
		$elem.addClass("selected");
		this.$el.find(".c_item:not(.selected)").remove();
	}
});

CategoryCahnnelFormView = Backbone.View.extend({
	events: {
		'submit #categoryForm': 'submit',
		'click .submit': 'validate',
		'click .confirm': 'confirm',
		'click .cancel': 'cancel'
	},
	//to ensure we use use a dark color on the color piker, max = 255 * 3; min = 0;use rgb colors
	maxClear : 385,
	// to ensure the random generated color is no so dark
	maxDark:150,
	initialize: function (options) {
		this._templateHTML = templates.category_channelForm;
		if(options.modelName === "channel"){
			this.collection = app.models.Channels;
			this.modelName = "Channel";
		}else{
			this.collection = app.models.Categories;
			this.modelName = "Campaign";
		}
	},
	render: function () {
		var self = this;
		if (!this.el.hasChildNodes()) {
			//syncs collection with server the first time when we open the modal
			this.collection.fetch();
		}

		if (this.model) {
			this.$el.hide(300,function(){
				self.$el.empty();
				var x = Mustache.render(self._templateHTML, {model:self.model,modelName:self.modelName});
				self.$el.append(x);
				self.$el.show(300);
			});
		}else{
			this.$el.empty();
			this.$el.append(Mustache.render(this._templateHTML,{modelName:this.modelName}));
			this.colorPiker = this.$el.find("#color");

			var total = 255 * 3;
			var a,b,c;
			var x =0;
			var maxDark = 100;
			while(total > this.maxClear || total < this.maxDark){
				//add + 1 to avoid get
				a = Math.floor(Math.random()*200)+1;
				b = Math.floor(Math.random()*200)+1;
				c = Math.floor(Math.random()*200)+1;
				total = a + b + c;
				console.log("color total:"+ total);
				console.log("color loop :"+ ++x);

			}
			var r = toHex(a);
			var g = toHex(b);
			var b = toHex(c);
			var color = '#'+r+g+b;
			console.log("color:#"+r+"-"+g+"-"+b);

			this.colorPiker.spectrum({
				color: color
			});
		}
		return this;
	},
	validate: function (e) {
		//e.preventDefault();
		var self = this;

		//Validate unique category
		var name = encodeURIComponent(this.$el.find("#name").val().toLowerCase());
		var cat = this.collection.findWhere({name:name});
		if (cat) {
			//if category exist then throw a validation error
			self.$el.find("#name").setCustomValidity("Please select a different name.");
		} else {
			self.$el.find("#name").setCustomValidity("");
		}


		//validate color is dark
		self.$el.find("#color").attr("style", "display:block");
		var color = this.colorPiker.spectrum('get').toHexString().toUpperCase();
		var r = parseInt(color.substring(1, 3), 16);
		var g = parseInt(color.substring(3, 5), 16);
		var b = parseInt(color.substring(5, 7), 16);
		var sum = r + g + b;
		if (sum >= this.maxClear) {
			self.$el.find("#color").setCustomValidity("This color is not dark enough.");
		} else {
			self.$el.find("#color").setCustomValidity("");
		}


	},
	submit: function (e) {
		e.preventDefault();
		this.model = {
			name: encodeURIComponent(this.$el.find("#name").val().toLowerCase()),
			color: this.colorPiker.spectrum('get').toHexString().toUpperCase()
		};
		this.render();
	},
	confirm: function () {
		this.collection.create(this.model,{
			wait : true,
			error: function (err) {
				console.log(err);
				alert('There was an error on the server, please try later.');
			}
		});
	},
	cancel: function () {
		var  m = this.model;
		this.model = null;
		this.render();
		this.$el.find("#name").val(decodeURIComponent(m.name));
		this.colorPiker.spectrum({
			color: m.color
		});
	}

});
function toHex(int){
	var x = int.toString(16);
	if(x.length < 2)
		x = "0"+x;
	return x;
}

/**
 * Created by marlon.jerez on 16/06/2015.
 */

var CalendarView = Backbone.View.extend({
	defaults: {},
	events: {
		'click .close': 'close',
		'click .bg': 'close'
	},
	initialize: function () {
		// bind the functions 'add' and 'remove' to the view.
		_(this).bindAll('add', 'remove', 'change');
		// init & bind this view to the agencies collection!
		this.collection.bind('add', this.add);
		this.collection.bind('remove', this.remove);
		this.collection.bind('change', this.change);

		//render existing elements
		this.$el.empty();
		this.collection.each(this.add);
	},
	render: function () {
		if (!this.el.hasChildNodes()) {
			this.$el.fullCalendar({
				header: {
					left: 'prev,next today ',
					center: 'title',
					right: 'month,basicWeek,basicDay'
				},
				eventClick: function (event, jsEvent, view) {
					var view = app.views.eventForm;
					if (!view)
						throw Error("app.views.eventForm not initialized");
					var event = app.models.Events.get(event.id);

					if (view.getEvent()) {
						if (event.id == view.getEvent().id)//same event displaying currently
							return;
					}
					view.setEvent(event);
					view.eventInfo();
				},
				dayClick: function (date, jsEvent, view) {
					var diff = moment().startOf("day").diff(date.startOf("day"));
					if (diff >= 0)
						return;
					var view = app.views.eventForm;
					if (!view)
						throw Error("app.views.eventForm not initialized");
					if (!view.eventView) {
						view.eventCreate(null, date, function () {
							view.resetDate();
						});
					} else if (view.eventView === "create") {
						return;
						view.$el.find("#from_date").val(date.format("DD/MM/YYYY"));
						view.resetDate();
					}
				},
				eventRender: function (event, element) {
					var cat = " <span class='fc-category' style='background-color:"+event.category_color+"'>&nbsp;</span>";
					var channel = " <span class='fc-channel' style='background-color:"+event.channel_color+"'>&nbsp;</span>";
					$(element).find(".fc-title").append("<span class='fc-event-cn'>"+cat+channel+"</span>");
				}
			});
		}
		return this;
	},
	prepareEvent: function (event) {
		event = event.clone();
		event.set("title", event.get("name"));
		event.set("start", event.get("from_date"));
		event.set("end", event.get("to_date"));
		event.set("className", "app_event agency_" + event.get("agencyId") + " category_" + slugify(event.get("categoryId")) + " channel_" + event.get("socialChannelId"));
		event.set("category_color",app.models.Categories.get(event.get("categoryId")).get("color"));
		event.set("channel_color",app.models.Channels.get(event.get("socialChannelId")).get("color"));
		var agencyId = event.get("agencyId");
		var agency = app.models.Agencies.get(agencyId);
		if (agency) {
			event.set("color", agency.get("color"));
		}
		return event.toJSON();
	},
	add: function (event) {
		// add item to the calendar
		var json = this.prepareEvent(event);
		this.$el.fullCalendar('renderEvent', json, true);
	},
	remove: function (event) {
		//this.$el.find("#filter_" + agency.get("name")).remove();
		this.$el.fullCalendar('removeEvents', event.get("id"));
	},
	updateEvent: function (event, fc_event) {
		fc_event.start = event.get("from_date");
		fc_event.end = event.get("to_date");
		fc_event.className = "app_event agency_" + event.get("agencyId") + " category_" + event.get("categoryId") + " channel_" + event.get("socialChannelId");
		fc_event.category_color = app.models.Categories.get(event.get("categoryId")).get("color");
		fc_event.channel_color = app.models.Channels.get(event.get("socialChannelId")).get("color");
		var agencyId = event.get("agencyId");
		var agency = app.models.Agencies.get(agencyId);
		if (agency) {
			fc_event.color = agency.get("color");
		}
		return fc_event;
	},
	change: function (event) {
		// update an event in the calendar
		var fc_event = this.$el.fullCalendar('clientEvents', event.get("id"));
		if (fc_event.length == 1)
			this.$el.fullCalendar('updateEvent', this.updateEvent(event, fc_event[0]));
	}
});



/**
 * Created by marlon.jerez on 20/05/2015.
 */
var Event = Backbone.Model.extend({
	urlRoot: '/api/Tasks'
});
var EventsCollection = Backbone.Collection.extend({
	url: '/api/Tasks',
	model: Event
});

var EventFormView = Backbone.View.extend({
	events: {
		'click #categorySelect .c_item': 'categorySelected',
		'click #channelSelect .c_item': 'channelSelected',
		'click .reset_form': "reset",
		'click .eventCreate': "eventCreate",
		'click .eventEdit': "eventEdit",
		'click .eventInfo': "eventInfo",
		'click .eventDelete': "eventDelete",
		'click .eventCancel': "eventCancel",
		'click .cancel': "renderInfoMessage",
		'submit #taskForm': 'confirm',
		"change #from_date": "resetDate"
	},
	titles: {
		create: "Create Event &nbsp;<i class='fa fa-calendar'></i>",
		edit: "Edit Event  &nbsp;<i class='fa fa-info-circle'></i>",
		info: "Event Info  &nbsp;<i class='fa fa-info-circle'></i>"
	},
	initialize: function () {
		this._templateHTML = templates.eventForm;
		this._InfoTemplate = templates.infoEvents;
		this.disable = true;
	},
	setEvent: function (model) {
		this.model = model;
		this.modelAttribs = model.attributes;
	},
	getEvent: function () {
		return this.modelAttribs;
	},
	reset: function (e, from) {
		this.disabled = false;
		this.render(from);
	},
	eventCreate: function (e, from, callback) {
		this.eventView = "create";
		this.model = null;
		this.modelAttribs = null;
		this.disabled = false;
		this.fadeRender(from, callback);
	},
	eventEdit: function (e, data) {
		this.eventView = "edit";
		this.disabled = false;
		this.render(data);
	},
	eventInfo: function (e, data, callback) {
		this.eventView = "info";
		this.disabled = true;
		this.fadeRender(data, callback);
	},
	eventDelete: function (e, data) {
		this.eventView = "delete";
		this.confirm(e);
	},
	eventCancel: function (e) {
		this.eventView = "cancel";
		this.renderInfoMessage();
	},
	renderInfoMessage: function (data) {
		this.model = null;
		this.eventView = null;
		this.modelAttribs = null;
		this.disabled = true;
		this.$el.empty();
		this.$el.append(Mustache.render(this._InfoTemplate, data));
		return this;
	},
	render: function (from) {
		var self = this;
		this.$el.empty();
		var data = this.getRenderData(from);
		this.$el.append(Mustache.render(this._templateHTML, data));
		this.enableControls();
		return this;
	},
	fadeRender: function (data, callback) {
		var self = this;

		var fadeDisabled = false;
		if (fadeDisabled) {
			self.render(data);
			if (self.eventView == "create") self.$el.find("#name").focus();
			if (callback) callback();
		} else {
			this.$el.fadeOut(300, function () {
				self.render(data);
				self.$el.fadeIn(300, function () {
					if (self.eventView == "create") self.$el.find("#name").focus();
					if (callback) callback();
				});
			});
		}
	},
	getRenderData: function (from) {
		var data = {};
		data.disabled = this.disabled;
		data.categories = app.models.Categories.models;
		data.channels = app.models.Channels.models;
		if (this.eventView == "create") {
			data.title = this.titles.create;
			data.created_on = Date.now();
			data.creatorId = MY_USER.get("id");
			data.createControls = true;
			if (from)
				data.FROM = moment(from).format("DD/MM/YYYY");
		} else {
			data.title = (this.disabled) ? this.titles.info : this.titles.create;
			data.created_on = this.modelAttribs.created_on;
			data.creatorId = this.modelAttribs.creatorId;
			data.event = this.modelAttribs;
			data.FROM = moment(this.modelAttribs.from_date).format("DD/MM/YYYY");
			data.TO = moment(this.modelAttribs.to_date).format("DD/MM/YYYY");
			data.categoryName = app.models.Categories.get(this.modelAttribs.categoryId).get("name");
			data.socialChannelName = app.models.Channels.get(this.modelAttribs.socialChannelId).get("name");
		}
		if (this.eventView == "info")
			data.editControls = (this.modelAttribs) ? this.modelAttribs.agencyId == MY_USER.get("agencyId") : false;

		return data;
	},
	enableControls: function () {
		if (!this.disabled) {
			//this.categoriesF = new FilterView({
			//	el: this.$el.find("#categorySelect")[0],
			//	collection: app.models.Categories,
			//	selectMode: true
			//});
			//this.cahnnelF = new FilterView({
			//	el: this.$el.find("#channelSelect")[0],
			//	collection: app.models.Channels,
			//	selectMode: true
			//});
			if(this.model){
				this.$el.find("#socialChannelId option[value='"+this.model.get("socialChannelId")+"']").prop("selected",true);
				this.$el.find("#categoryId option[value='"+this.model.get("categoryId")+"']").prop("selected",true);
			}

			var now = moment();
			this.$el.find("#from_date").daterangepicker({
				format: 'DD/MM/YYYY',
				startDate: now,
				minDate: now,
				singleDatePicker: true,
				showDropdowns: true
			});
			this.$el.find("#to_date").daterangepicker({
				format: 'DD/MM/YYYY',
				startDate: now,
				minDate: now,
				singleDatePicker: true,
				showDropdowns: true
			});
		}
	},
	animateFrom: function ($from, $to) {
		$to.focus();
		$from.addClass("transition");
		window.setTimeout(function () {
			$from.removeClass("transition");
		}, 200);
	},
	resetDate: function (e) {
		var $from = this.$el.find("#from_date");
		var $to = this.$el.find("#to_date");
		//this.animateFrom($from,$to);
		var min = moment($from.val(), 'DD/MM/YYYY');
		$to.val("");
		$to.daterangepicker({
			format: 'DD/MM/YYYY',
			startDate: min,
			minDate: min,
			singleDatePicker: true,
			showDropdowns: true
		});
	},
	getFormData: function () {
		this.formData = transForm.serialize("#taskForm",{skipDisabled:false});
		var data = this.formData;
		data.categoryId = parseInt(data.categoryId,10);
		data.categoryName = app.models.Categories.get(data.categoryId).get("name");
		data.socialChannelId = parseInt(data.socialChannelId,10);
		data.socialChannelName = app.models.Channels.get(data.socialChannelId).get("name");
		data.memberId = parseInt(data.memberId,10);
		data.from_date = moment(data.from_date, 'DD/MM/YYYY').startOf('day').add(1, 'h').toDate();
		data.to_date = moment(data.to_date, 'DD/MM/YYYY').startOf('day').add(23, 'h').toDate();
		data.agencyId = MY_USER.get("agencyId");
		return this.formData;
	},
	channelSelected: function (e) {
		var $elem = $(e.currentTarget);
		$elem.attr("style", "");
		this.$el.find("#socialChannelId").val($elem.data("name"));
	},
	categorySelected: function (e) {
		var $elem = $(e.currentTarget);
		$elem.attr("style", "");
		var color = $elem.data("color");
		this.$el.find(".taskForm").css({
			"border-color": color
		});
		this.$el.find(".submit").css({
			"background-color": color
		});
		this.$el.find("#categoryId").val($elem.data("name"));
	},
	confirm: function (e) {
		e.preventDefault();
		var self = this;
		if (this.eventView == "create")
			var title = "<span class='text-primary'>Create Event</span>";
		if (this.eventView == "edit")
			var title = "<span class='text-primary'>Edit Event</span>";
		if (this.eventView == "delete")
			var title = "<span class='test-primary'>Delete Event</span>";

		var data = this.getFormData();
		//data.category = app.models.Agencies.get(data.categoryId).get("name");
		var content = Mustache.render(templates.eventConfirm, data);
		app.views.modal.showDialog(title, content, {
			confirm: function () {
				self.submit();
			},
			cancel: function () {
			}
		})
	},
	submit: function (e) {
		var self = this;
		var callbacks = {
			wait: true,    // waits for server to respond with 200 before adding newly created model to collection
			success: function (resp) {
				self.renderInfoMessage();
			},
			error: function (err) {
				self.renderInfoMessage();
				// this error message for dev only
				alert('There was an error on the server, please try later.');
			}
		};
		if (!MY_USER || !MY_USER.get("agencyId")) {
			alert("aplication error: MY_USER.agencyId not defined ");
			return;
		}

		if (this.model && this.eventView == "delete") {
			this.model.destroy(callbacks);
			return;
		}

		var data = this.getFormData();
		if (app.models.Events && this.eventView === "create") {
			data.created_on = new Date(Date.now());
			app.models.Events.create(data, callbacks);
		} else if (this.model && this.eventView == "edit") {
			this.model.save(data, callbacks);
		}
	}
});





/**
 * Created by marlon.jerez on 07/05/2015.
 */

FooterView = Backbone.View.extend({
	el: "#Footer",
	initialize: function () {
		this.position();
		_.bindAll(this, 'position');
		$(window).on("load",this.position);
		$(window).on("resize",this.position);
	},
	position: function () {
		var footer = this.$el;
		if(!footer) return; /// fix a bug where footer is not defined on page load
		var pos = footer.position();
		var height = $(window).height();
		height = height - pos.top;
		height = height - footer.height();
		if (height > 0) {
			footer.css({
				'margin-top': height + 'px'
			});
		}else{
			footer.css({
				'margin-top': 20 + 'px'
			});
		}
		footer.addClass("ready");
	}
});

/**
 * Created by marlon.jerez on 07/05/2015.
 */

/**
 * Created by Ma jerez on 24/05/2015.
 */
/**
 * Open a modal window
 * To open a modal just add the class .modal-trigger to any element.
 * The content of the modal window is automatically loaded reading the data attribute in four different ways:
 * 1 - data-template="templateName" => load one templates from templates.json.
 * 2 - data-url="http://abcd...."  => load the content from an url.
 * 3 - data-message="Hello this is the message" => load the modal and display the message.
 * 4 - data-selector="#ModalContent" => clone one element using the selector and call show() to display the
 *    element if it is hidden.
 *
 */
var ModalView = Backbone.View.extend({
	$modal: null,
	$content: null,
	centered: false,
	initialize: function () {
		$("body").append(templates.modal);
		this.$modal = $("#Modal");
		this.$content = this.$modal.find(".modal-content");
		_.bindAll(this, 'moodalTrigger','dialogConfirm','dialogCancel' , "modalClose");
		$(document).on("click", ".modal-trigger", this.moodalTrigger);
		$(document).on("click", "#Modal .dialogConfirm", this.dialogConfirm);
		$(document).on("click", "#Modal .dialogCancel", this.dialogCancel);
		this.$modal.on("hidden.bs.modal",this.modalClose);
		this._modalMessage = templates.modalMessage;
	},
	show: function () {
		var self = this;
		this.center();
		this.$modal.modal("show");
		if (this.close_timeout >= 0) {
			window.setTimeout(function () {
				self.$modal.modal('hide');
			}, this.close_timeout)
		}
	},
	setSize: function (size) {
		if (size === "large") {
			this.$modal.find(">.modal-dialog").attr("class", "modal-dialog modal-lg");
		} else if (size === "small") {
			this.$modal.find(">.modal-dialog").attr("class", "modal-dialog modal-sm");
		} else {
			this.$modal.find(">.modal-dialog").attr("class", "modal-dialog");
		}
	},
	setCentered: function (centered) {
		this.centered = centered;
	},
	modalClose: function (e) {
		this.setCentered(false);
		this.setSize("medium");
	},
	setCloseTimeout: function (time) {
		this.close_timeout = parseInt(time);
	},
	// this must be called after the content has been appended to this.$content
	center: function () {
		if (this.centered) {
			var $clone = this.$modal.clone().css('display', 'block').appendTo('body');
			var top = Math.round(($clone.height() - $clone.find('.modal-content').height()) / 2);
			top = top > 0 ? top : 0;
			$clone.remove();
			this.$content.css("margin-top", top);
		} else {
			this.$content.css("margin-top", 0);
		}
	},
	/**
	 * Automatically shows a modal window loading the conten
	 */
	moodalTrigger: function (e) {
		e.preventDefault();
		this.$content.empty();
		this.close_timeout = 0;
		var $elem = $(e.currentTarget);

		// content load method
		var template = $elem.data("template");
		var url = $elem.data("url");
		var message = $elem.data("message");
		var title = $elem.data("title");
		var selector = $elem.data("selector");

		//config parameters
		var size = $elem.data("size");
		var close_timeout = $elem.data("close-timeout");
		var centered = $elem.data("centered");
		this.setCentered(centered);
		this.setSize(size);
		this.setCloseTimeout(close_timeout);

		//select load methid
		if (template)
			this.showTemplate(template);
		else if (url)
			this.showURL(url);
		else if (message)
			this.showMessage( title,message);
		else if (selector)
			this.showDOMelement(selector);
		else
			this.showTriggerError();

	},
	/**
	 * Shows a view on a modal window.
	 * it call view.render() and then append view.$el to the modal window
	 */
	showView: function (data, view) {
		this.$content.empty();
		view.render(data);
		this.$content.append(view.$el);
		this.show();
	},
	showTemplate: function (templateName) {
		this.$content.empty();
		if (templates[templateName]) {
			this.$content.append(templates[templateName]);
		} else {
			this.$content.append(templates["modalError"]);
		}
		this.$modal.modal('show');
	},
	showURL: function (url) {
		this.$content.empty();
		$.ajax({
			url: url,
			success: function (data) {
				this.$content.append(data);
				this.show();
			},
			error: function () {
				this.$content.append(templates["modalError"]);
				this.show();
			}
		});
	},
	showMessage: function (title, message) {
		this.$content.empty();
		this.$content.append(Mustache.render(this._modalMessage, {message: message, title: title}));
		this.show();
	},
	showDialog: function (title ,message, callbacks) {
		if(callbacks){
			this.confirmCallback = callbacks.confirm;
			this.cancelCallback = callbacks.cancel;
		}
		this.$content.empty();
		this.$content.append(Mustache.render(this._modalMessage, {message: message, title: title,dialog:true}));
		this.show();
	},
	showDOMelement: function (selector) {
		this.$content.empty();
		var content = $(selector);
		if (content.length) {
			this.$content.append(content.clone().show());
		} else {
			this.$content.append(templates["modalError"]);
		}
		this.show();
	},
	showTriggerError: function () {
		// wrong modal trigger configuration
		this.setSize("normal");
		this.setCentered(true);
		this.setCloseTimeout(3000);

		var title = "Modal button not configured properly";
		var message =
			"<strong>data-url:</strong> loads the contem fron and ajax request.<br/>" +
			"<strong>data-template</strong> loads the contem from /templates/templates.json.<br/>" +
			"<strong>data-selector</strong> loads the contem froma an elemnt on the dom ie: '#MY-modal-conten'.<br/>" +
			"<strong>data-message.</strong> print the value of the attributes data-message and data-title (optional).<br/>";
		this.showMessage(message, title);
	},
	dialogConfirm:function(e){
		if(this.confirmCallback && typeof this.confirmCallback === 'function')
			this.confirmCallback(e);
	},
	dialogCancel: function (e) {
		if(this.cancelCallback && typeof this.cancelCallback === 'function')
			this.cancelCallback(e);
	}

});

var NavView = Backbone.View.extend({
	el: '#PageNav',
	defaults: {
	},
	events: {
		'click .close': 'close',
		'click .bg': 'close'
	},
	initialize: function() {
		this._templateHTML = templates.modal;
		this.render();
	}
});

var User = Backbone.Model.extend({
	urlRoot: '/api/Members',
	/**
	 * get a list of tokens on the server for this user
	 * @param callback
	 */
	tokens: function (callback) {
		var self = this;
		if (!self.get("id")) {
			//no id = unAuthenticated, doesn't have tokens
			callback("unAuthenticated user");
			return;
		}
		var url = '/api/Members/' + this.get("id") + "/accessTokens";
		$.ajax({
			method: "GET",
			dataType: "json",
			url: url,
			success: function (data, textStatus, jqxhr) {
				callback(null, data);
			},
			error: function (jqxhr, textStatus, error) {
				callback(jqxhr.responseJSON.error, null);
			}
		});
	},
	/**
	 * Check if  credentials are valid (app_userId = MY_USER.id)
	 * If callback is provided make a query to the server and return error if
	 * the user is not authenticated.
	 */
	isAuthenticated: function (callback) {
		var app_userId = window.readCookie("app_userId");
		var logged = (app_userId && app_userId === this.get("id")+"");
		if(callback){
			// query the api to check that user has valid tokens
			this.tokens(function (err) {
				callback(err);
			});
		}
		// returns true if "app_userId" cookie is the same as the id of this user
		return logged;
	}
});

var UserCollection = Backbone.Collection.extend({
	url: '/api/Members',
	model: User
});


var LoginView = Backbone.View.extend({
	events: {
		'click .close': 'close',
		'submit #form-login': 'login'
	},
	$form: null,
	formData: null,
	initialize: function () {
		this._templateHTML = templates.login;
	},
	render: function (m) {
		if (!this.el.hasChildNodes()) {
			//avoid re-render if render() is called multiple times
			//initialize selectors
			this.$el.append(Mustache.render(this._templateHTML, m));
			this.$form = this.$el.find("#form-login");
		}
		this.$el.find("#login-again").hide();
		this.$el.find(".error").hide();
		return this;
	},
	renderLoginAgain: function () {
		this.$el.find("#login-again").show();
	},
	getFormData: function () {
		this.formData = transForm.serialize(this.$form.selector);
		return this.formData;
	},
	login: function (e) {
		if (e)
			e.preventDefault();
		var self = this;
		var d = this.getFormData();
		$.ajax({
			method: "POST",
			dataType: "json",
			url: '/api/Members/login',
			data: {email: d.email, password: d.password, rememberMe: d.rememberMe},
			success: function (data) {
				$(document).trigger("logged-in");
				var app_userId = window.readCookie("app_userId");
				MY_USER = new User({id: app_userId});
				MY_USER.fetch();
				if(d.successUrl){
					window.location.href = "/#"+d.successUrl;
					window.location.reload();
					//app.navigate(d.successUrl, {trigger: true});
				}

			},
			error: function (jqxhr) {
				self.$el.find(".error").slideDown();
			}
		});
	}
},{
	logout: function (callback) {
		$.ajax({
			method: "POST",
			dataType: "json",
			url: '/api/Members/logout',
			success: function (data, textStatus, jqxhr) {
				$(document).trigger("logged-out");
				callback();
			},
			error: function (jqxhr, textStatus, error) {
				callback(error);
			}
		});
	},
});


/**
 * Add login routes to the main APP
 */
$(document).on("AppReady", function () {
	app.views.login = new LoginView();
	app.route("login", "login", function () {
		app.addView("#content", "login");
	});
	app.route("logout", "logout", function () {
		LoginView.logout(function(err){
			if(err){
				alert("log-out error: " + error);
			}else{
				app.navigate("login", {trigger: true});
			}
		});
	});
});

/**
 * initializes the  MY_USER
 */
function init_MY_USER(callback){
	var app_userId = window.readCookie("app_userId");
	MY_USER = new User({id: app_userId});
	MY_USER.isAuthenticated(function(err){
		//auth == true if the user logged in is present
		if(err){
			createCookie('app_userId', '', 1);
			if (Backbone.History.started && APP) {
				// if backbone history estarted then navigate to login page
				app.navigate("login", {trigger: true});
			} else {
				// if backbone history bot estarted then change hash to login page
				// and login page will be displayed once
				window.location.hash = "login";
			}
		}else{
			MY_USER.fetch();
		}
		callback();
	});
}


