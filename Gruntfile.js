/* ##### CONFIG VARS ##### */

var srcPath = './src/'; // always end the path with an slash '/'
var compilePath = './http_public/assets/'; // always end the path with an slash '/'


/* ##### WORDPRESS compilePath = theme directory  ##### */
//var themeName = "wpTheme-by-radical";
//var compilePath = 'wordpress/wp-content/themes/' + themeName + '/assets/'; // always end the path with an slash '/'


/* ##### GRUNT ##### */
require('colors');
module.exports = function (grunt) {

	if( grunt.option('admin') ){
		console.log("############## Grunt for admin app ############");
		srcPath = srcPathAdmin;
		compilePath = compilePathAdmin;
	}
	// Project configuration.
	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),


		// Console prompt tasks
		prompt: {}, // cofigured dinamically.

		// image optimization
		imagemin: { // Task
			live: { // Another target
				files: [{
					expand: true, // Enable dynamic expansion
					flatten:true, // remove images path
					cwd: srcPath, // Src matches are relative to this path
					src: ['*/images/*.{png,jpg,gif,jpeg}'], // Actual patterns to match
					dest: compilePath + 'images/' // Destination path prefix
				}]
			}
		},
		//concatenates js files
		concat: {
			js_libs: {
				src: getJs_libs(), // generated fron 'concatJSdevDependencies' in the bower.json file
				dest: compilePath + 'js/libs.js'
			},
			js_main: {
				src: [
					srcPath + '*/*.js'
				],
				dest: compilePath + 'js/main.js'
			}
		},
		//minify previous concatenated javascript
		uglify: {
			js_libs: {
				options: {
					report: 'min',
					sourceMap: false
//					sourceMapIncludeSources:true,
//					sourceMapIn : compilePath + 'js/main.js.map'
				},
				src: [compilePath + 'js/libs.js'],
				dest: compilePath + 'js/libs.min.js'
			},
			js_main: {
				options: {
					report: 'min',
					sourceMap: true
//					sourceMapIncludeSources:true,
//					sourceMapIn : compilePath + 'js/libs.js.map'
				},
				src: [compilePath + 'js/main.js'],
				dest: compilePath + 'js/main.min.js'
			}
		},
		// compile  sass (//TODO pending to configure ans join with less)
		sass: { // Task
			main: {
				options: { // Target options
					style: 'expanded'
				},
				src: [srcPath + 'sass/style.scss'],
				dest: compilePath + 'css/style.css'
			}
		},

		less: {
			main: {
				src: srcPath + '_Application/style.less',
				dest: compilePath + 'css/style.css'
			}
		},
		// set prexises automatically  to css
		autoprefixer: {
			options: {
				browsers: ['> 1%', 'last 3 versions', "ie 8", 'ie 9']
			},
			main: {
				src: compilePath + 'css/style.css',
				dest: compilePath + 'css/style.css'
			}
		},
		cssmin: {
			options: {
				sourceMap: false
			},
			main: {
				src: compilePath + 'css/style.css',
				dest: compilePath + 'css/style.min.css'
			}
		},

		// watch for file modifications and run respective tasks
		watch: {
			sass: {
				files: [srcPath + "**/*.scss"],
				tasks: ["sass", "autoprefixer", "cssmin"]
			},
			less: {
				files: [srcPath + "**/*.less"],
				tasks: ["less", "autoprefixer", "cssmin"]
			},
			js_libs: {
				files: ["./bower_components/**/*.js"],
				tasks: ["concat:js_libs", "uglify:js_libs"]
			},
			js_main: {
				files: [ srcPath + "**/*.js"],
				tasks: ["concat:js_main", "uglify:js_main"]
			},
			imagemin: {
				files: srcPath + "images/**/*.{png,jpg,gif,jpeg}",
				tasks: ["imagemin"]
			},
			templates: {
				files: srcPath + "**/*.html",
				tasks: ["template-compile"]
			}
		}
	});

	/* ################## LOAD NPM ################## */

	grunt.loadNpmTasks('grunt-contrib-concat');
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-contrib-sass');
	grunt.loadNpmTasks('grunt-contrib-less');
	grunt.loadNpmTasks('grunt-contrib-cssmin');
	grunt.loadNpmTasks('grunt-contrib-imagemin');
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-autoprefixer');


	/* ################## TASKS ################## */

	grunt.registerTask('frontend-sass',
		["imagemin", "sass", "autoprefixer", "cssmin", "template-compile", "concat", "uglify", "watch"]
	);

	grunt.registerTask('frontend-less',
		["imagemin", "less", "autoprefixer", "cssmin", "template-compile", "concat", "uglify", "watch"]
	);



	grunt.registerTask('template-compile', "#hide# Compiles Mustache Templates to Json.", function () {
		var templatesDir = srcPath;
		var fs = require('fs');
		var path = require('path');
		var dirs = fs.readdirSync(templatesDir);
		var tpl = {};
		for (var dirIndex in dirs) {
			var dir = path.join( templatesDir,dirs[dirIndex]);
			if (fs.lstatSync(dir).isDirectory()) {
				var files = fs.readdirSync(dir);
				for (var fileindex in files) {
					var file = path.join( dir,files[fileindex]);
					if (/.html$/.test(file)) {
						grunt.log.debug(file);
						var tplName = files[fileindex].slice(0, files[fileindex].length - 5);
						tpl[tplName] = fs.readFileSync(file, {
							encoding: 'utf8'
						});
					}
				}
			}
		}

		grunt.file.write(compilePath + "templates/templates.json", JSON.stringify(tpl, null, "\t"));
		console.log("File '" + compilePath + "templates/templates.json' Created");
	});

	/* ################## UTIL FUNCTIONS ################## */


	/* Reads the concatJSdevDependencies in bower.json and
	 * Generates an object to use in the concat task as config.
	 * example of the returned object:
	 js_libs: {
	 src: [
	 'bower_components/jquery/jquery.js',
	 'bower_components/jquery/jquery-migrate.js',
	 'bower_components/mustache/mustache.js',
	 'bower_components/underscore/underscore.js',
	 'bower_components/backbone/backbone.js',
	 ],
	 dest: ....,
	 },
	 */
	function getJs_libs() {
		var bower = grunt.file.readJSON('bower.json');
		var dependenciesList = bower.concatJSdevDependencies;
		var src = [];
		for (var dependency in dependenciesList) {
			var filesArray = dependenciesList[dependency];
			for (var file in filesArray)
				src.push(dependency + filesArray[file]);
		}
		return src;
	}

};
