module.exports = function(SocialChannel) {
	SocialChannel.validatesUniquenessOf('name', {message: 'name is not unique'});
};
